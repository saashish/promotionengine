dotnet test

coverlet bin\Debug\netcoreapp3.1\PromotionEngine.Test.dll --target "dotnet" --targetargs "test --no-build" --format opencover

reportgenerator "-reports:coverage.opencover.xml" "-targetdir:coveragereport" -reporttypes:Html