﻿using PromotionEngine.Promotions;
using Shouldly;
using System;
using System.Collections.Generic;
using Xunit;

namespace PromotionEngine.Test.Promotions
{
    public class PromotionEngineTest
    {
        private const float aCostPrice = 50;
        private const uint aQuantity = 1;

        private const float bCostPrice = 30;
        private const uint bQuantity = 1;

        private const float cCostPrice = 20;
        private const uint cQuantity = 1;

        [Theory]
        [MemberData(nameof(InvalidSkus))]
        public void ShouldThrowArgumentExceptionWhenInvalidSKUsProvided(IEnumerable<SKU> skus)
        {
            IPromotionEngine promotionEngine = new PromotionEngine.Promotions.PromotionEngine();

            IList<IPromotion> promotions = new List<IPromotion>();

            Should.Throw<ArgumentException>(() => promotionEngine.Checkout(skus, promotions)).Message.ShouldBe("Invalid skus.");
        }

        [Theory]
        [MemberData(nameof(InvalidPromotions))]
        public void ShouldReturnTotalPriceAsCostPriceWhenInvalidPromotionsProvided(IEnumerable<IPromotion> promotions)
        {
            IPromotionEngine promotionEngine = new PromotionEngine.Promotions.PromotionEngine();

            IList<SKU> skus = new List<SKU>(3)
            {
                new SKU('A', aQuantity, aCostPrice),
                new SKU('B', bQuantity, bCostPrice),
                new SKU('C', cQuantity, cCostPrice)
            };

            float actual = promotionEngine.Checkout(skus, promotions);

            actual.ShouldBe((aCostPrice * aQuantity) + (bCostPrice * bQuantity) + (cCostPrice * cQuantity));
        }

        [Theory]
        [MemberData(nameof(NonMatchingPromotion))]
        public void ShouldReturnTotalPriceAsZeroWhenPromotionNotFound(IEnumerable<IPromotion> promotions, float expected)
        {
            IPromotionEngine promotionEngine = new PromotionEngine.Promotions.PromotionEngine();

            IList<SKU> skus = new List<SKU>(2)
            {
                new SKU('A', aQuantity, aCostPrice),
                new SKU('B', bQuantity, bCostPrice)
            };

            float actual = promotionEngine.Checkout(skus, promotions);

            actual.ShouldBe(expected);
        }

        [Theory]
        [MemberData(nameof(Promotions))]
        public void ShouldReturnTotalPriceWhenPromotionFound(IEnumerable<IPromotion> promotions, float expected)
        {
            IPromotionEngine promotionEngine = new PromotionEngine.Promotions.PromotionEngine();

            SKU duplicateSkuForA = new SKU('A', 5, aCostPrice);
            duplicateSkuForA.AddPromotionType(PromotionType.NSkuItemsForFixedPrice);

            IList<SKU> skus = new List<SKU>(5)
            {
                new SKU('A', 5, aCostPrice),
                new SKU('B', 5, bCostPrice),
                duplicateSkuForA,
                new SKU('C', cQuantity, cCostPrice),
                new SKU('D', cQuantity, cCostPrice)
            };

            float actual = promotionEngine.Checkout(skus, promotions);

            actual.ShouldBe(expected);
        }

        [Fact]
        public void ShouldReturnTotalPriceForScenerioA()
        {
            IPromotionEngine promotionEngine = new PromotionEngine.Promotions.PromotionEngine();

            IList<SKU> skus = new List<SKU>(3)
            {
                new SKU('A', aQuantity, aCostPrice),
                new SKU('B', bQuantity, bCostPrice),
                new SKU('C', cQuantity, cCostPrice),
            };

            IList<IPromotion> promotions = new List<IPromotion>(3)
            {
                new NItemsForFixedPrice(3, 130, new List<char>(1) { 'A' }),
                new NItemsForFixedPrice(2, 45, new List<char>(1) { 'B' }),
                new NSkusForFixedPrice(1, 30, new List<char>(2) { 'C', 'D' })
            };

            float actual = promotionEngine.Checkout(skus, promotions);

            const float expected = 100;

            actual.ShouldBe(expected);
        }

        [Fact]
        public void ShouldReturnTotalPriceForScenerioB()
        {
            IPromotionEngine promotionEngine = new PromotionEngine.Promotions.PromotionEngine();

            IList<SKU> skus = new List<SKU>(3)
            {
                new SKU('A', 5, aCostPrice),
                new SKU('B', 5, bCostPrice),
                new SKU('C', cQuantity, cCostPrice),
            };

            IList<IPromotion> promotions = new List<IPromotion>(3)
            {
                new NItemsForFixedPrice(3, 130, new List<char>(1) { 'A' }),
                new NItemsForFixedPrice(2, 45, new List<char>(1) { 'B' }),
                new NSkusForFixedPrice(1, 30, new List<char>(2) { 'C', 'D' })
            };

            float actual = promotionEngine.Checkout(skus, promotions);

            const float expected = 370;

            actual.ShouldBe(expected);
        }

        [Fact]
        public void ShouldReturnTotalPriceForScenerioC()
        {
            IPromotionEngine promotionEngine = new PromotionEngine.Promotions.PromotionEngine();

            IList<SKU> skus = new List<SKU>(3)
            {
                new SKU('A', 3, aCostPrice),
                new SKU('B', 5, bCostPrice),
                new SKU('C', cQuantity, 0),
                new SKU('C', cQuantity, 30)
            };

            IList<IPromotion> promotions = new List<IPromotion>(3)
            {
                new NItemsForFixedPrice(3, 130, new List<char>(1) { 'A' }),
                new NItemsForFixedPrice(2, 45, new List<char>(1) { 'B' }),
                new NSkusForFixedPrice(1, 30, new List<char>(2) { 'C', 'D' })
            };

            float actual = promotionEngine.Checkout(skus, promotions);

            const float expected = 280;

            actual.ShouldBe(expected);
        }

        public static TheoryData<IEnumerable<SKU>> InvalidSkus => new TheoryData<IEnumerable<SKU>>
        {
            new List<SKU>()
        };

        public static TheoryData<IEnumerable<IPromotion>> InvalidPromotions => new TheoryData<IEnumerable<IPromotion>>
        {
            new List<IPromotion>()
        };

        public static TheoryData<IEnumerable<IPromotion>, float> NonMatchingPromotion()
        {
            TheoryData<IEnumerable<IPromotion>, float> nonMatchingPromotions = new TheoryData<IEnumerable<IPromotion>, float>();

            IPromotion nItemsForFixedPrice = GetNItemsForFixedPrice(3, 130, new List<char> { 'C' });
            nonMatchingPromotions.Add(new List<IPromotion>(1) { nItemsForFixedPrice }, 80);

            return nonMatchingPromotions;
        }

        public static TheoryData<IEnumerable<IPromotion>, float> Promotions()
        {
            TheoryData<IEnumerable<IPromotion>, float> promotions = new TheoryData<IEnumerable<IPromotion>, float>();

            IPromotion nItemsForFixedPriceForFirstProduct = GetNItemsForFixedPrice(3, 130, new List<char>(1) { 'A' });
            IPromotion nItemsForFixedPriceForSecondProduct = GetNItemsForFixedPrice(2, 45, new List<char>(1) { 'B' });
            IPromotion nItemsForFixedPriceForThirdProduct = GetNSkusForFixedPrice(1, 30, new List<char>(2) { 'C', 'D' });

            promotions.Add(new List<IPromotion>(1) { nItemsForFixedPriceForFirstProduct }, 230 + 150 + 0 + 20 + 20);
            promotions.Add(new List<IPromotion>(1) { nItemsForFixedPriceForSecondProduct }, 250 + 120 + 0 + 20 + 20);
            promotions.Add(new List<IPromotion>(2) { nItemsForFixedPriceForFirstProduct, nItemsForFixedPriceForSecondProduct }, 230 + 120 + 0 + 20 + 20);

            promotions.Add(new List<IPromotion>(3) { nItemsForFixedPriceForFirstProduct, nItemsForFixedPriceForSecondProduct, nItemsForFixedPriceForThirdProduct }, ((1 * 130) + (2 * 50)) + ((2 * 45) + (1 * 30)) + (1 * 30));

            return promotions;
        }

        private static IPromotion GetNItemsForFixedPrice(uint minimumQuantity, float discountPrice, IEnumerable<char> skuIds)
        {
            IPromotion nItemsForFixedPrice = new NItemsForFixedPrice(minimumQuantity, discountPrice, skuIds);

            return nItemsForFixedPrice;
        }

        private static IPromotion GetNSkusForFixedPrice(uint minimumQuantity, float discountPrice, IEnumerable<char> skuIds)
        {
            IPromotion nItemsForFixedPrice = new NSkusForFixedPrice(minimumQuantity, discountPrice, skuIds);

            return nItemsForFixedPrice;
        }
    }
}
