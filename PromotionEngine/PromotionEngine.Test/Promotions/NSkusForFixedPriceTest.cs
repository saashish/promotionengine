﻿using PromotionEngine.Promotions;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace PromotionEngine.Test.Promotions
{
    public class NSkusForFixedPriceTest
    {
        [Theory]
        [MemberData(nameof(InvalidSKUs))]
        public void ShouldReturnZeroWhenInvalidSKUsProvided(IEnumerable<SKU> skus)
        {
            IPromotion promotion = new NSkusForFixedPrice(1, 30, new List<char>(2) { 'C', 'D' });

            float actual = promotion.Apply(skus);

            const float expected = 0;

            actual.ShouldBe(expected);

            if (skus.Any())
            {
                skus.First().PromotionTypes.Contains(PromotionType.NSkusForFixedPrice).ShouldBeFalse();
            }
        }

        [Fact]
        public void ShouldReturnZeroWhenPromotionTypeIsNotEmpty()
        {
            IPromotion promotion = new NSkusForFixedPrice(3, 130, new List<char>(1) { 'A' });

            SKU sku = new SKU('A', 5, 0);
            sku.AddPromotionType(PromotionType.NSkuItemsForFixedPrice);

            IList<SKU> skus = new List<SKU>(1) { sku };

            float actual = promotion.Apply(skus);

            const float expected = 0;

            actual.ShouldBe(expected);
            skus.First().PromotionTypes.Contains(PromotionType.NSkusForFixedPrice).ShouldBeFalse();
        }

        [Theory]
        [MemberData(nameof(ValidSingleSKUs))]
        public void ShouldReturnZeroWhenValidSingleSKUsProvided(IEnumerable<SKU> skus, float expected)
        {
            IPromotion promotion = new NSkusForFixedPrice(3, 130, new List<char>(1) { 'A' });

            float actual = promotion.Apply(skus);

            actual.ShouldBe(expected);
            skus.First().PromotionTypes.Contains(PromotionType.NSkusForFixedPrice).ShouldBeFalse();
        }

        [Theory]
        [MemberData(nameof(MultipleValidSKUs))]
        public void ShouldReturnNonZeroWhenMultipleValidSKUsProvided(IEnumerable<SKU> skus, float expected)
        {
            IPromotion promotion = new NSkusForFixedPrice(2, 30, new List<char>(2) { 'C', 'D' });

            float actual = promotion.Apply(skus);

            actual.ShouldBe(expected);

            IEnumerable<PromotionType> promotionTypes = skus.SelectMany(y => y.PromotionTypes);

            promotionTypes.Count().ShouldBe(skus.Count());

            foreach (PromotionType promotionType in promotionTypes)
            {
                promotionType.ShouldBe(PromotionType.NSkusForFixedPrice);
            }
        }

        [Theory]
        [MemberData(nameof(MultipleInvalidSKUs))]
        public void ShouldReturnNonZeroWhenMultipleInvalidSKUsProvided(IEnumerable<SKU> skus, float expected)
        {
            IPromotion promotion = new NSkusForFixedPrice(2, 30, new List<char>(2) { 'C', 'D' });

            float actual = promotion.Apply(skus);

            actual.ShouldBe(expected);
        }

        public static TheoryData<IEnumerable<SKU>, float> ValidSingleSKUs => new TheoryData<IEnumerable<SKU>, float>
        {
            {  new List<SKU>(1) { new SKU ('A', 5, 50) }, 0 },
            {  new List<SKU>(1) { new SKU ('A', 3, 50) }, 0 }
        };

        public static TheoryData<IEnumerable<SKU>, float> MultipleValidSKUs => new TheoryData<IEnumerable<SKU>, float>
        {
            { new List<SKU>(2) { new SKU ('C', 2, 0), new SKU('D', 2, 0), }, 30 },
            { new List<SKU>(2) { new SKU ('D', 2, 0), new SKU('C', 2, 0), }, 30 }
        };

        public static TheoryData<IEnumerable<SKU>, float> MultipleInvalidSKUs => new TheoryData<IEnumerable<SKU>, float>
        {
            { new List<SKU>(2) { new SKU('C', 1, 0), new SKU('D', 2, 0), }, 0 },
            { new List<SKU>(2) { new SKU('E', 1, 0), new SKU('F', 1, 0), }, 0 }
        };

        public static TheoryData<IEnumerable<char>> SkuIds => new TheoryData<IEnumerable<char>>
        {
            new List<char>(),
            new List<char>(1) { 'A' },
            new List<char>(2) { 'A', 'B' }
        };

        public static TheoryData<IEnumerable<SKU>> InvalidSKUs => new TheoryData<IEnumerable<SKU>>
        {
            new List<SKU>(),
            new List<SKU>(1) { new SKU('A', 1, 0) },
            new List<SKU>(2) { new SKU('A', 1, 0), new SKU('B', 3, 0) }
        };
    }
}
