﻿using PromotionEngine.Promotions;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace PromotionEngine.Test.Promotions
{
    public class NItemsForFixedPriceTest
    {
        [Theory]
        [MemberData(nameof(InvalidSKUs))]
        public void ShouldReturnZeroWhenInvalidSKUsProvided(IEnumerable<SKU> skus)
        {
            IPromotion promotion = new NItemsForFixedPrice(3, 130, new List<char>(1) { 'A' });

            float actual = promotion.Apply(skus);

            const float expected = 0;

            actual.ShouldBe(expected);

            if (skus.Any())
            {
                skus.First().PromotionTypes.ShouldBeEmpty();
            }
        }

        [Fact]
        public void ShouldReturnZeroWhenPromotionTypeIsNotEmpty()
        {
            IPromotion promotion = new NItemsForFixedPrice(3, 130, new List<char>(1) { 'A' });

            SKU sku = new SKU('A', 5, 0);
            sku.AddPromotionType(PromotionType.NSkusForFixedPrice);

            IList<SKU> skus = new List<SKU>(1) { sku };

            float actual = promotion.Apply(skus);

            const float expected = 0;

            actual.ShouldBe(expected);
            skus.First().PromotionTypes.Contains(PromotionType.NSkuItemsForFixedPrice).ShouldBeFalse();
        }

        [Theory]
        [MemberData(nameof(ValidSingleSKUs))]
        public void ShouldReturnNonZeroWhenValidSingleSKUsProvided(IEnumerable<SKU> skus, float expected)
        {
            IPromotion promotion = new NItemsForFixedPrice(3, 130, new List<char>(1) { 'A' });

            float actual = promotion.Apply(skus);

            actual.ShouldBe(expected);
            skus.First().PromotionTypes.Contains(PromotionType.NSkuItemsForFixedPrice).ShouldBeTrue();
        }

        [Theory]
        [MemberData(nameof(MultipleValidSKUs))]
        public void ShouldReturnZeroWhenMultipleSKUsProvided(IEnumerable<SKU> skus, float expected)
        {
            IPromotion promotion = new NItemsForFixedPrice(3, 130, new List<char>(2) { 'A', 'B' });

            float actual = promotion.Apply(skus);

            actual.ShouldBe(expected);
            skus.First().PromotionTypes.Contains(PromotionType.NSkuItemsForFixedPrice).ShouldBeFalse();
        }

        public static TheoryData<IEnumerable<SKU>> InvalidSKUs => new TheoryData<IEnumerable<SKU>>
        {
            new List<SKU>(),
            new List<SKU>(1) { new SKU ('A', 1, 0) },
            new List<SKU>(1) { new SKU ('B', 3, 0) },
            new List<SKU>(1) { new SKU ('C', 5, 0) }
        };

        public static TheoryData<IEnumerable<SKU>, float> ValidSingleSKUs => new TheoryData<IEnumerable<SKU>, float>
        {
            {  new List<SKU>(1) { new SKU ('A', 5, 50) }, 230 },
            {  new List<SKU>(1) { new SKU ('A', 3, 50) }, 130 }
        };

        public static TheoryData<IEnumerable<SKU>, float> MultipleValidSKUs => new TheoryData<IEnumerable<SKU>, float>
        {
            {  new List<SKU>(1) { new SKU ('A', 5, 50) }, 0 },
            {  new List<SKU>(1) { new SKU ('B', 3, 50) }, 0 }
        };

        public static TheoryData<IEnumerable<char>> SkuIds => new TheoryData<IEnumerable<char>>
        {
            new List<char>(),
            new List<char>(1) { 'A' },
            new List<char>(2) { 'A', 'B' },
        };
    }
}
