﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PromotionEngine.Promotions
{
    public class PromotionEngine : IPromotionEngine
    {
        public float Checkout(IEnumerable<SKU> skus, IEnumerable<IPromotion> promotions)
        {
            if (!skus.Any())
            {
                throw new ArgumentException("Invalid skus.");
            }

            if (!promotions.Any())
            {
                return skus.Sum(x => GetCost(x));
            }

            float total = promotions.Sum(x => x.Apply(skus));

            foreach (SKU sku in skus.Where(x => !x.PromotionTypes.Any()))
            {
                total += GetCost(sku);
            }

            return total;
        }

        private float GetCost(SKU sku)
        {
            return sku.Quantity * sku.Price;
        }
    }
}
