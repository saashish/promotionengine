﻿namespace PromotionEngine.Promotions
{
    public enum PromotionType : byte
    {
        NSkuItemsForFixedPrice = 0,
        NSkusForFixedPrice = 1
    }
}
