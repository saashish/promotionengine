﻿using System.Collections.Generic;

namespace PromotionEngine.Promotions
{
    public interface IPromotion
    {
        uint MinimumQuantity { get; }

        IEnumerable<char> SkuIds { get; }

        float DiscountedPrice { get; }

        PromotionType Type { get; }

        float Apply(IEnumerable<SKU> skus);
    }
}
