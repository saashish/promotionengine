﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PromotionEngine.Promotions
{
    public class NSkusForFixedPrice : IPromotion
    {
        public NSkusForFixedPrice(uint minimumQuantity, float discountedPrice, IEnumerable<char> skuIds)
        {
            MinimumQuantity = minimumQuantity;
            DiscountedPrice = discountedPrice;
            SkuIds = new HashSet<char>(skuIds);
        }

        public uint MinimumQuantity { get; }

        public IEnumerable<char> SkuIds { get; }

        public float DiscountedPrice { get; }

        public PromotionType Type => PromotionType.NSkusForFixedPrice;

        public float Apply(IEnumerable<SKU> skus)
        {
            float totalPrice = 0;

            if (SkuIds.Count() <= 1)
            {
                return totalPrice;
            }

            IEnumerable<SKU> applicableSkus = skus.Where(x => !x.PromotionTypes.Any() && SkuIds.Contains(x.Id) && x.Quantity >= MinimumQuantity);

            if (!applicableSkus.Any() || applicableSkus.Count() != SkuIds.Count())
            {
                return totalPrice;
            }

            totalPrice += DiscountedPrice;

            foreach (SKU applicableSku in applicableSkus)
            {
                applicableSku.AddPromotionType(Type);
            }

            return totalPrice;
        }
    }
}
