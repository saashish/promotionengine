﻿using System.Collections.Generic;

namespace PromotionEngine.Promotions
{
    public interface IPromotionEngine
    {
        float Checkout(IEnumerable<SKU> skus, IEnumerable<IPromotion> promotions);
    }
}
