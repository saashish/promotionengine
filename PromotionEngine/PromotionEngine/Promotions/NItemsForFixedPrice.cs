﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PromotionEngine.Promotions
{
    public class NItemsForFixedPrice : IPromotion
    {
        public NItemsForFixedPrice(uint minimumQuantity, float discountedPrice, IEnumerable<char> skuIds)
        {
            MinimumQuantity = minimumQuantity;
            DiscountedPrice = discountedPrice;
            SkuIds = new HashSet<char>(skuIds);
        }

        public uint MinimumQuantity { get; }

        public IEnumerable<char> SkuIds { get; }

        public float DiscountedPrice { get; }

        public PromotionType Type => PromotionType.NSkuItemsForFixedPrice;

        public float Apply(IEnumerable<SKU> skus)
        {
            IEnumerable<SKU> applicableSkus = skus.Where(x => !x.PromotionTypes.Any() && HasSingleSku() && SkuIds.Contains(x.Id) && x.Quantity >= MinimumQuantity);

            float totalPrice = 0;

            if (!applicableSkus.Any())
            {
                return totalPrice;
            }

            foreach (SKU applicableSku in applicableSkus)
            {
                totalPrice += GetPrice(applicableSku);

                applicableSku.AddPromotionType(Type);
            }

            return totalPrice;
        }

        private bool HasSingleSku()
        {
            return SkuIds.Count() == 1;
        }

        private float GetPrice(SKU applicableSku)
        {
            return GetDiscountedPrice(applicableSku.Quantity) + GetCostPrice(applicableSku.Quantity, applicableSku.Price);
        }

        private float GetDiscountedPrice(uint quantity)
        {
            uint discountedQuantity = quantity / MinimumQuantity;

            return discountedQuantity * DiscountedPrice;
        }

        private float GetCostPrice(uint quantity, float costPrice)
        {
            uint nonDiscountedQuantity = quantity % MinimumQuantity;

            return nonDiscountedQuantity * costPrice;
        }
    }
}
