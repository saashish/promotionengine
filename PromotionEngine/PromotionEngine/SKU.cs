﻿using PromotionEngine.Promotions;
using System.Collections.Generic;

namespace PromotionEngine
{
    public class SKU
    {
        private readonly IList<PromotionType> promotionTypes = new List<PromotionType>();

        public SKU(char id, uint quantity, float price)
        {
            Id = id;
            Quantity = quantity;
            Price = price;
        }

        public char Id { get; }

        public uint Quantity { get; }

        public float Price { get; }

        public IEnumerable<PromotionType> PromotionTypes => promotionTypes;

        public void AddPromotionType(PromotionType promotionType)
        {
            promotionTypes.Add(promotionType);
        }
    }
}
